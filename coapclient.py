import asyncio
from aiocoap import *

async def coapgetlampstatus(url):
    # print('coapgetlampstatus on', url)
    protocol = await Context.create_client_context()
    request = Message(code=GET, uri=url)
    try:
        response = await protocol.request(request).response
    except Exception as e:
        print('Failed to fetch resource:', e)
        return None # If no lamp connection is available, return None
    else:
        print('Result: %s\n%r'%(response.code, response.payload))
        return response.payload


async def coapsetlampstatus(url, value):
    # print('coapsetlampstatus on', url, 'with value', value)
    protocol = await Context.create_client_context()
    request = Message(code=PUT, uri=url, payload=value)

    try:
        response = await protocol.request(request).response
    except Exception as e:
        print('Failed to fetch resource:', e)

