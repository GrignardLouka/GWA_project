#Aanmaken centraal beheerde 'app' module waarbij alle bibliotheken worden geconfigureerd en opgestart.
#Aparte module vermijdt problemen rond circulair importeren

import flask, flask_login
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from decouple import config

login_manager = flask_login.LoginManager()
db = SQLAlchemy()
bcrypt = Bcrypt()

lamps = [
        "1a",
        "1b",
        "1c",
        "2a",
        "2b",
        "2c",
        "3a",
        "3b",
        "3c",
        "4a",
        "4b",
        "4c",
        "5a",
        "5b",
        "5c",
    ]

def create_app():
    app = flask.Flask(__name__)
    app.secret_key = config("SECRET_KEY")
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///database.db"
    
    login_manager.init_app(app)
    db.init_app(app)
    bcrypt.init_app(app)    

    return app
