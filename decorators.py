from functools import wraps
from flask import render_template, redirect, url_for
from flask_login import current_user
from app import login_manager

from tables import User, TimeTable

# region ------------------------ flask handlers -------------------------------

# Called everytime a page is requested or api call is made
# This is to ensure the user has access at the time of the request
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


# flask-login custom unauthorized handler. Wordt uitgevoerd indien niet ingelogd of ongeldig...
@login_manager.unauthorized_handler
def unauthorized():
    return render_template(
        "root.html", message="Unautherized access, please login."
    )


# endregion


# region ------------------------ custom decorators -------------------------------
def user_access(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print("CHECK USER ACCESS")
        if not TimeTable.access_check(current_user.id):
            print("NO ACCESS RETURN")
            return render_template(
                "home.html",
                message="no access during this period, speak to the admin if you need access."
            )
        else:
            print("ACCESS")
            return func(*args, **kwargs)

    return wrapper


# endregion
