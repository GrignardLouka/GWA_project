from flask import render_template
from tables import User
from app import lamps

# region --------------------------- form input --------------------------------
def input_parsing_register(username, password, email):
    check = input_parsing_username(username, register=True)
    if not check[0]:
        return [
            False,
            render_template("register.html", message=check[1]["error"]),
        ]
    # Check password
    if len(password) > 30:
        return  render_template("register.html", message="password must be less then 30 characters")

    if User.is_email_taken(email):
        return render_template("register.html", message="email is already taken")
    

    return [True]

def input_parsing_login(username, password):
    check = input_parsing_username(username)
    if not check[0]:
        return [
            False,
            render_template("login.html", message=check[1]["error"]),
        ]
    # Check password
    if len(password) > 30:
        return  render_template("register.html", message="password must be less then 30 characters")

    return [True]


def input_parsing_username(username, register=False):
    # Check if username is less then 30 characters
    if len(username) > 30:
        return [False, {"error": "username must be less then 30 characters"}]
    # Check if username only contains letters
    for i in range(len(username)):
        if (
            username[i]
            not in "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        ):
            return [
                False,
                {"error": "username must be only letters and numbers"},
            ]

    if register:
        if User.is_username_taken(username):
            return [False, {"error": "username is already taken"}]
        else:
            return [True, {"username": username}]
    else:
        if User.is_username_taken(username):
            return [True, {"username": username}]
        else:
            return [False, {"error": "username does not exist"}]





# endregion

# region ------------------------------ api -------------------------------------
def input_parsing_value(value):
    """function to check if value you want to set your lamp at
    is an integer between 0 and 100

    Args:
        value int: a value you want to set your lamp to

    Returns:
        list: with the first element being a boolean saying if the value
        is legit and the second element being an dict with an error message
        in case of value not being legit
    """
    try:
        value = int(value)
    except:
        return [False, {"error": "value is not an integer"}]
    if value > 100:
        return [
            False,
            {"error": "Value is above 100, it has to be between 0 and 100"},
        ]
    elif value < 0:
        return [
            False,
            {"error": "Value is below 0, it has to be between 0 and 100"},
        ]
    return [True]


def input_parsing_lamp(lamp):
    """function to check if the selected lamp exists

    Args:
        lamp string: name of the lamp you want to select

    Returns:
        bool: true if it is a valid lamp that is in a hardcoded list, else false
    """
    if lamp in lamps:
        return True
    return False


# endregion
