# region Libraries
from aiocoap import *
from flask import render_template, request, redirect, url_for
from flask_login import current_user, login_user, logout_user, login_required

# endregion

# region  Split files
from app import create_app, login_manager, db, bcrypt, lamps
from coapclient import coapgetlampstatus, coapsetlampstatus
from input_parsing import (
    input_parsing_login,
    input_parsing_register,
    input_parsing_value,
    input_parsing_lamp,
)
from tables import User, TimeTable
from decorators import user_access, load_user, unauthorized

# endregion

app = create_app()
message_var = ""


# region ---------------------------  routes -----------------------------------
@app.route("/")
def root():
    try:
        if current_user.is_authenticated:
            return redirect(url_for("home"))
    except:
        pass
    global message_var
    mes = message_var
    message_var = ""
    return render_template("root.html", message=mes)


@app.route("/login", methods=["GET", "POST"])
def login():
    global message_var
    if request.method == "POST":
        try:
            # Read inputs
            username = request.form["username"]
            pwd = request.form["password"]
            print(username)
            print(type(username))
            # input parsing
            check = input_parsing_login(username, pwd)
            if not check[0]:
                return check[1]

            # Get user from database based on username from form input
            user = User.query.filter_by(username=username).first()

            # check if user exists and if the passwords match
            if user and user.pwd and bcrypt.check_password_hash(user.pwd, pwd):
                login_user(user)
            else:
                message_var = "Invalid password"
                return redirect(url_for("login"))

            return redirect(url_for("home"))
        except Exception as e:
            print(e)
            return e
    else:
        mes = message_var
        message_var = ""
        return render_template("login.html", message=mes)


@app.route("/register", methods=["GET", "POST"])
def register():
    if request.method == "POST":
        try:
            username = request.form["username"]
            pwd = request.form["password"]
            email = request.form["email"]
            department = request.form["department"]
            role = request.form["role"]

            # Check inputs
            check = input_parsing_register(username, pwd, email)
            if not check[0]:
                return check[1]

            pwd_hashed = bcrypt.generate_password_hash(pwd).decode("utf-8")

            User.add_new_user(username, pwd_hashed, email, department, role)

            return redirect(url_for("login"))

        except Exception as e:
            print(e)
            return e
    else:
        return render_template("register.html")


@login_required
@app.route("/logout")
def logout():
    logout_user()
    global message_var
    message_var = "You have been logged out."
    return redirect(url_for("root"))


@login_required
@app.route("/home")
def home():
    return render_template("home.html")


@app.route("/control")
@login_required
def control():
    return render_template("control.html")


# endregion


# region -------------------------- api routes ---------------------------------


@app.route("/api/all/", methods=["GET", "POST", "PUT"])
@login_required
@user_access
async def alllampstatus():
    if request.method == "GET":
        arr = {}
        for lamp in lamps:
            val = await coapgetlampstatus(
                "coap://lamp" + str(lamp) + ".irst.be/lamp/dimming"
            )
            if val:
                arr[lamp] = val.decode()
            else:
                arr[lamp] = 20

        if arr[min(arr)] == arr[max(arr)]:
            arr["all"] = arr[lamps[0]]
        else:
            arr["all"] = "/"
        return arr

    if request.method == "POST" or "PUT":
        # get input value
        value = request.form["values"]

        value_status = input_parsing_value(value)
        if not value_status[0]:
            return value_status[1]

        for lamp in lamps:
            await coapsetlampstatus(
                "coap://lamp" + str(lamp) + ".irst.be/lamp/dimming", value
            )
        return {"result": "OK"}


@app.route("/api/<lamp>", methods=["GET", "POST", "PUT"])
@login_required
@user_access
async def lampstatus(lamp):
    """Function that can be called throught the API.

    on GET it return the value of the selected lamp.
    this function has input parsing to check if the lamp name that is called
    is legit.

    on POST or PUT it changes the value of the selected lamp.
    this function has input parsing to check the lamp and the input value.
    depending on what the lamp name is you can either change 1 specific lamp
    or all the lamps.

    Args:
        lamp string: selected lamp

    Returns:
        GET
        dict: the value for the selected lamp

        POST or PUT
        dict: either a confirmation or an error message.
    """
    # Check if lamp exists else return error
    if not input_parsing_lamp(lamp):
        return {"error": "lamp does not exist"}

    # IF GET
    if request.method == "GET":
        # Get lamp value
        val = await coapgetlampstatus(
            "coap://lamp" + str(lamp) + ".irst.be/lamp/dimming"
        )

        if val:
            return {"lamp": lamp, "value": (str(val.decode()))}
        else:
            return {"lamp": lamp, "value": "/"}

    # IF POST OR PUT
    elif request.method == "POST" or request.method == "PUT":
        # get input value and check wether its legit
        value = request.form["values"]
        value_status = input_parsing_value(value)
        if not value_status[0]:
            return value_status[1]

        await coapsetlampstatus(
            "coap://lamp" + str(lamp) + ".irst.be/lamp/dimming",
            str.encode(value),
        )
        return {"result": "OK"}

    # UNSUPPORTED METHOD
    else:
        return ({"error": "not supported"}, 405)


# endregion


# Run the app
if __name__ == "__main__":
    app.run(host="localhost", port=8080)
