import os

#Uitvoeren om de sqlite database en bijhorende tabellen te initialiseren aan de hand van de gedefinieerde modellen.
#via: python manage-db.py
from app import create_app,db

# This file may look unused but without it the database will be empty
import tables

def init_db():
    # If database already exists, delete it and create a new one
    if os.path.exists("instance/database.db"):
        os.remove("instance/database.db")
    else:
        print("No previous database found")
        
    
    """Initialize database(s)."""
    app = create_app()
    app.app_context().push()
    db.create_all()

	
if __name__ == "__main__":
    init_db()
