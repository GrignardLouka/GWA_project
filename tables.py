# Bevat de database modellen.

from app import db
from flask_login import UserMixin

from datetime import datetime

# De gebruikte 'User' class wordt als subclass voortgebouwd bovenop:
# - flask-login UserMixin: bevat de benodigde flask-login zaken
# - db.model (komend van SQLAlchemy): ORM database mapping


class User(UserMixin, db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), unique=True, nullable=False)
    pwd = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(80), unique=True, nullable=False)
    department = db.Column(db.String(80), nullable=False)
    role = db.Column(db.String(30), nullable=False)

    @classmethod
    def is_username_taken(cls, username):
        return db.session.query(
            db.exists().where(User.username == username)
        ).scalar()

    @classmethod
    def is_email_taken(cls, email):
        return db.session.query(
            db.exists().where(User.email == email)
        ).scalar()

    @classmethod
    def add_new_user(cls, username, pwd, email, department, role):
        new_user = User(
            username=username,
            pwd=pwd,
            email=email,
            department=department,
            role=role,
        )
        db.session.add(new_user)
        db.session.commit()


class TimeTable(UserMixin, db.Model):
    __tablename__ = "TimeTable"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    start = db.Column(db.DateTime, nullable=False)
    end = db.Column(db.DateTime, nullable=False)

    @classmethod
    def add_new_time(cls, user_id, start, end):
        new_time = TimeTable(
            user_id=user_id,
            start=start,
            end=end,
        )
        db.session.add(new_time)
        db.session.commit()

    @classmethod
    def access_check(cls, user_id):
        date = datetime.now()

        result = db.session.query(
            db.exists().where(
                TimeTable.user_id == user_id,
                TimeTable.start <= date,
                TimeTable.end >= date,
            )
        ).scalar()

        print(result)

        return result
    
    
    
    
    


class LogBook(UserMixin, db.Model):
    __tablename__ = "LogBook"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    lamp = db.Column(db.String, nullable=False)
    initial_value = db.Column(db.Integer, nullable=False)
    new_value = db.Column(db.Integer, nullable=False)
    